<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>formulario</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">


<style>
	body{
		background: #ffe259;
		background: linear-gradient(to right, #ffa751, #ffe259);
	}
	.bg{
		background-image: url(https://previews.123rf.com/images/dixi_/dixi_1512/dixi_151200123/50274564-chiot-teckel-mensonge-et-en-regardant-la-cam%C3%A9ra-isol%C3%A9-sur-blanc-.jpg);
		background-position: center center;
	}
</style>
</head>

<body>

	<div class="container rounded w-75 mt-5 shadow">
		<div class="row align-items-stretch">
			<div class="col bg d-none d-lg-block">

			</div>
			<div class="col bg-white p-5 rounded-end">
				<div class="text-end">
					<img src="https://econotec.com.bo/wp-content/uploads/2021/08/cropped-LogoSR.png" width="48" alt="">
				</div>
				<h2 class="text-center">Formulario</h2>
				<form action="../controllers/formulario.php" method="POST">
					<div class="mb-4">
						<div class="form-group">
							<label for="name">Nombre Completo</label>
							<input type="text" name="name" id="name" class="form-control">
						</div>
					</div>
					<div class="mb-4">
						<label for="phone">Telefono</label>
						<input type="text" name="phone" id="phone" class="form-control">
					</div>
					<div class="mb-4">
						<label for="email">Correo Electronico</label>
						<input type="email" name="email" id="email" class="form-control">
					</div>
					<div class="d-grid">
						<button type="submit" class="btn btn-primary">Enviar</button>
					</div>
				</form>
			</div>
		</div>
		



		<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>

</html>